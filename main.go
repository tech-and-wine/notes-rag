package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/parakeet-nest/parakeet/completion"
	"github.com/parakeet-nest/parakeet/embeddings"
	"github.com/parakeet-nest/parakeet/llm"
	"github.com/xanzy/go-gitlab"
)

func main() {

	gitLabCli, gitLabCliErr := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"), gitlab.WithBaseURL("https://gitlab.com"))
	if gitLabCliErr != nil {
		log.Fatal("😡 Failed to create client:", gitLabCliErr)
	}

	projectId := 58580220
	issueId := 1
	// https://gitlab.com/tech-and-wine/documents/-/issues/1
	notes, _, gitLabNotesErr := gitLabCli.Notes.ListIssueNotes(projectId, issueId, nil)

	if gitLabNotesErr != nil {
		log.Fatal("😡 Failed to get the notes:", gitLabNotesErr)
	}

	ollamaUrl := "http://localhost:11434"
	model := "phi3:mini"
	embeddingsModel := "all-minilm" // This model is for the embeddings of the documents

	store := embeddings.MemoryVectorStore{
		Records: make(map[string]llm.VectorRecord),
	}

	systemContent := `You are an helpful agent. 
	Your duty is to provide clear and concise answers with the help of the provided context.`

	var docs []string

	for _, note := range notes {
		docs = append(docs, note.Body)
	}

	// Create embeddings from documents and save them in the store
	for idx, doc := range docs {
		fmt.Println("Creating embedding from document ", idx)
		embedding, err := embeddings.CreateEmbedding(
			ollamaUrl,
			llm.Query4Embedding{
				Model:  embeddingsModel,
				Prompt: doc,
			},
			strconv.Itoa(idx),
		)
		if err != nil {
			fmt.Println("😡:", err)
		} else {
			store.Save(embedding)
		}
	}

	userContent := `Who is James T Kirk?.`
	//userContent := `Who is Philippe Charrrière?`

	// Create an embedding from the question
	embeddingFromQuestion, err := embeddings.CreateEmbedding(
		ollamaUrl,
		llm.Query4Embedding{
			Model:  embeddingsModel,
			Prompt: userContent,
		},
		"question",
	)
	if err != nil {
		log.Fatalln("😡:", err)
	}
	fmt.Println("🔎 searching for similarity...")

	similarity, _ := store.SearchMaxSimilarity(embeddingFromQuestion)

	fmt.Println("🎉 similarity", similarity)

	documentsContent := `<context><doc>` + similarity.Prompt + `</doc></context>`

	options := llm.Options{
		Temperature:   0.5, // default (0.8)
		RepeatLastN:   2,   // default (64) the default value will "freeze" deepseek-coder
		RepeatPenalty: 1.5,
	}

	query := llm.Query{
		Model: model,
		Messages: []llm.Message{
			{Role: "system", Content: systemContent},
			{Role: "system", Content: documentsContent},
			{Role: "user", Content: userContent},
		},
		Options: options,
		Stream:  true,
	}

	_, chatErr := completion.ChatStream(ollamaUrl, query,
		func(answer llm.Answer) error {
			fmt.Print(answer.Message.Content)
			return nil
		})

	if chatErr != nil {
		log.Fatal("😡:", chatErr)
	}

}
